import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_uas/models/GooglePlaceSearchModel.dart';

class GooglePlaceSearchController {
  final String query;
  GooglePlaceSearchController({@required this.query});

  Future<GooglePlaceSearchModel> getSearchModel() async {
    Dio dio = Dio();
    print(
        "https://maps.googleapis.com/maps/api/place/textsearch/json?query=${Uri.encodeQueryComponent(this.query)}&key=AIzaSyDSO2nxyhuJqFVVC-Q-4hT1c9FgGRfV_Ek");
    try {
      Response response = await dio.get(
          "https://maps.googleapis.com/maps/api/place/textsearch/json?query=${Uri.encodeQueryComponent(this.query)}&key=AIzaSyDSO2nxyhuJqFVVC-Q-4hT1c9FgGRfV_Ek");
      if (response.statusCode == 200) {
        return GooglePlaceSearchModel.fromJson(response.data);
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
