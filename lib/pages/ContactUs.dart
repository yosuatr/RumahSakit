import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_uas/models/DeveloperContactModel.dart';
import 'package:flutter_uas/pages/ConfirmationPage.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final FirebaseDatabase _database = FirebaseDatabase.instance;

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String submit = '';
  final myControllerNama = TextEditingController();
  final myControllerEmail = TextEditingController();
  final myControllerPesan = TextEditingController();
  @override
  void dispose() {
    myControllerNama.dispose();
    myControllerEmail.dispose();
    myControllerPesan.dispose();
    super.dispose();
  }

  simpanForm() {
    DeveloperContactModel developerContactModel = new DeveloperContactModel(
        myControllerNama.text, myControllerEmail.text, myControllerPesan.text);
    _database
        .reference()
        .child("developerContact")
        .push()
        .set(developerContactModel.toJson());
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Developer Contact'),
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.all(35.0),
            child: Form(
              key: _formkey,
              child: Column(
                children: [
                  TextFormField(
                    controller: myControllerNama,
                    validator: (value) => (myControllerNama.text.isEmpty)
                        ? 'Nama tidak boleh dikosongkan'
                        : null,
                    decoration: InputDecoration(
                      hintText: 'Masukan Nama',
                      labelText: 'Nama',
                      icon: Icon(Icons.person),
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  Container(height: 10.0),
                  TextFormField(
                    controller: myControllerEmail,
                    decoration: InputDecoration(
                      hintText: 'email@Gmail.com',
                      labelText: 'Email',
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(),
                    ),
                    validator: (value) {
                      Pattern pattern =
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                      RegExp regex = RegExp(pattern);
                      if (!regex.hasMatch(value))
                        return 'Email Anda tidak valid';
                      else
                        return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                  ),
                  Container(height: 10.0),
                  TextFormField(
                    controller: myControllerPesan,
                    validator: (value) => (myControllerPesan.text.isEmpty)
                        ? 'Pesan tidak boleh dikosongkan'
                        : null,
                    decoration: InputDecoration(
                      hintText: 'Masukan Pesan',
                      labelText: 'Pesan',
                      icon: Icon(Icons.message),
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.text,
                  ),
                  Container(height: 10.0),
                  RaisedButton(
                    color: Colors.blue,
                    child: Text('Submit'),
                    onPressed: () {
                      if (_formkey.currentState.validate()) {
                        simpanForm();
                        var route = MaterialPageRoute(
                          builder: (BuildContext context) => HalamanKonfirmasi(
                            nama: myControllerNama.text,
                            email: myControllerEmail.text,
                            pesan: myControllerPesan.text,
                          ),
                        );
                        Navigator.of(context).push(route);
                      }
                    },
                  ),
                  Text(this.submit),
                ],
              ),
            )),
      ),
    );
  }
}
