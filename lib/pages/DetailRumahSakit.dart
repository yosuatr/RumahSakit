import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_uas/helpers/MapUtils.dart';
import 'package:flutter_uas/models/GooglePlaceSearchModel.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DetailRumahSakit extends StatefulWidget {
  final Result candidate;

  DetailRumahSakit({this.candidate});

  @override
  _DetailRumahSakitState createState() => _DetailRumahSakitState();
}

class _DetailRumahSakitState extends State<DetailRumahSakit> {
  Completer<GoogleMapController> _controller = Completer();
  Iterable markers = [];
  Set<Marker> _markers = {};

  void setMapPins() {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId('source'),
          position: LatLng(widget.candidate.geometry.location.lat,
              widget.candidate.geometry.location.lng),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue)));
    });
  }

  void onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    setMapPins();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        title: Text("Maps ${widget.candidate.name} "),
      ),
      body: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: LatLng(widget.candidate.geometry.location.lat,
                widget.candidate.geometry.location.lng),
            zoom: 14.0,
          ),
          markers: _markers,
          onMapCreated: onMapCreated),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          MapUtils.openMap(widget.candidate.geometry.location.lat,
              widget.candidate.geometry.location.lng);
        },
        child: Icon(Icons.navigation),
        backgroundColor: Colors.blue[300],
        mini: true,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
