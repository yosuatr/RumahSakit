import 'package:flutter/material.dart';
import 'package:flutter_uas/controllers/GooglePlaceSearchController.dart';
import 'package:flutter_uas/models/GooglePlaceSearchModel.dart';
import 'package:flutter_uas/pages/DetailRumahSakit.dart';

class ListRumahSakitPage extends StatefulWidget {
  final String query;
  ListRumahSakitPage({@required this.query});

  @override
  _ListRumahSakitPageState createState() => _ListRumahSakitPageState();
}

class _ListRumahSakitPageState extends State<ListRumahSakitPage> {
  List<Result> candidate = [];
  String _sortType = "highest";

  getRumahSakit() async {
    GooglePlaceSearchController _gpc =
        GooglePlaceSearchController(query: widget.query);

    GooglePlaceSearchModel _rumahSakit = await _gpc.getSearchModel();

    setState(() {
      candidate = _rumahSakit.results;
    });
  }

  sortList() {
    setState(() {
      if (_sortType == "highest") {
        candidate.sort((b, a) => a.rating.compareTo(b.rating));
      } else {
        candidate.sort((a, b) => a.rating.compareTo(b.rating));
      }
    });
  }

  @override
  void initState() {
    getRumahSakit();
    super.initState();
  }

  void _settingModalBottomSheet(context) async {
    var sort = await showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.thumb_up),
                    title: new Text('Rating Tertinggi'),
                    onTap: () {
                      Navigator.pop(context, "highest");
                    }),
                new ListTile(
                  leading: new Icon(Icons.thumb_down),
                  title: new Text('Rating Terendah'),
                  onTap: () {
                    Navigator.pop(context, "lowest");
                  },
                ),
              ],
            ),
          );
        });

    setState(() {
      _sortType = sort;
    });

    sortList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.query}".toUpperCase()),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: candidate.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.all(8.0),
              child: ListTile(
                title: Text("${candidate.elementAt(index).name}"),
                subtitle: Text("Rating ${candidate.elementAt(index).rating}"),
                trailing: Icon(Icons.add_location),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailRumahSakit(
                                candidate: candidate.elementAt(index),
                              )));
                },
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _settingModalBottomSheet(context);
        },
        label: Text('Sort'),
        icon: Icon(Icons.sort),
        backgroundColor: Colors.pink,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
