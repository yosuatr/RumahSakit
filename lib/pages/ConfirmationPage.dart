import 'package:flutter/material.dart';

class HalamanKonfirmasi extends StatefulWidget {
  final String nama;
  final String email;
  final String pesan;

  HalamanKonfirmasi({Key key, this.nama, this.email, this.pesan})
      : super(key: key);

  @override
  _HalamanKonfirmasiState createState() => _HalamanKonfirmasiState();
}

class _HalamanKonfirmasiState extends State<HalamanKonfirmasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Halaman Konfirmasi'),
      ),
      body: Column(children: [
        Container(height: 35.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Nama: ${widget.nama}'),
          ],
        ),
        Container(height: 25.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Email: ${widget.email}'),
          ],
        ),
        Container(height: 25.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Pesan: ${widget.pesan}'),
          ],
        )
      ]),
    );
  }
}
