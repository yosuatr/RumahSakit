import 'package:flutter/material.dart';
import 'package:flutter_uas/pages/ContactUs.dart';
import 'package:flutter_uas/pages/ListRumahSakit.dart';

void main() => runApp(Home());

class Home extends StatelessWidget {
  final judul = 'Cari Rumah Sakit';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: judul,
      home: HalamanUtama(title: judul),
    );
  }
}

class HalamanUtama extends StatelessWidget {
  final String title;

  HalamanUtama({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Cari Rumah Sakit"), backgroundColor: Colors.blue[300]),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/abc.jpg"),
            RaisedButton(
              child: Text('MULAI CARI'),
              color: Colors.blue[300],
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Cari()));
              },
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(''),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/menu.jpg'),
                ),
                color: Colors.blue[300],
              ),
            ),
            ListTile(
              title: Text('Home'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()));
              },
            ),
            ListTile(
              title: Text('Cari Rumah Sakit'),
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Cari()));
              },
            ),
            ListTile(
              title: Text('Developer Contact'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ContactUs()));
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Cari extends StatefulWidget {
  @override
  _CariState createState() => _CariState();
}

class _CariState extends State<Cari> {
  String valuegol;

  void pilihan() {
    String _query = "rumah sakit ${valuegol}";
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListRumahSakitPage(query: _query)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Cari Rumah Sakit',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.blue[300],
        ),
        body: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.all(35.0),
              child: Form(
                child: Column(children: [
                  Center(
                    child: DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('Mata'),
                          value: 'mata',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Jantung'),
                          value: 'jantung',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('THT'),
                          value: 'tht',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Kulit'),
                          value: 'kulit',
                        ),
                      ],
                      onChanged: (String value) {
                        setState(() {
                          valuegol = value;
                        });
                      },
                      hint: Text('Pilih Kategori'),
                      value: valuegol,
                    ),
                  ),
                  Container(height: 10.0),
                  RaisedButton(
                    color: Colors.blue[300],
                    child: Text('Cari'),
                    onPressed: () {
                      pilihan();
                    },
                  ),
                ]),
              )),
        ));
  }
}
