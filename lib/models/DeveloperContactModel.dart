import 'package:firebase_database/firebase_database.dart';

class DeveloperContactModel {
  String key;
  String nama;
  String email;
  String pesan;
  DeveloperContactModel(this.nama, this.email, this.pesan);

  DeveloperContactModel.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        nama = snapshot.value["nama"],
        email = snapshot.value["email"],
        pesan = snapshot.value["pesan"];

  toJson() {
    return {"nama": nama, "email": email, "pesan": pesan};
  }
}
